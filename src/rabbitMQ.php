<?php
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class rabbitMQ {
	const RABBIT_SERVER= '__RABBIT_SERVER__';
	const RABBIT_PORT= '__RABBIT_PORT__'; //  default 5672
	const RABBIT_USER='__RABBIT_USERNAME__';
	const RABBIT_PASS='__RABBIT_PASSWORD__';
	private $user=NULL;
	private $pass=NULL;
	private $server=NULL; // servidor rabbitmq
	private $port=NULL; // puerto rabbitMQ
	private $tagName=NULL; // cola de servicio
	private $exchange=NULL; // intercambio
	private $estatus=false;
	private $socket=NULL;
	private $canal=NULL;
	private $error=NULL;
	private $sucess=NULL;

	/**
	* establece el servidor servicio de rabbit
	* @param string $a direccion del server
	*/
	public function setServer($a=false) {
		$this->server= ($a ? $a : self::RABBIT_SERVER);
	}

	/**
	* retorn el servidor servicio de rabbit
	* @return string direccion del server
	*/
	public function getServer() {
		return $this->server;
	}

	/**
	* establece el puerto del servidor servicio de rabbit
	* @param string $a puerto del direccion del server
	*/
	public function setPort($a=false) {
		$this->port= ($a ? $a : self::RABBIT_PORT);
	}

	/**
	* retorn el puerto del servidor servicio de rabbit
	* @return string puerto del server
	*/
	public function getPort() {
		return $this->port;
	}

	/**
	* establece estado del servicio de rabbit
	* @param boolean $a falso o verdadero
	*/
	public function setSatus($a=false) {
		$this->estatus= $a;
	}

	/**
	* retorna estado del servicio de rabbit
	* @return boolean falso o verdadero
	*/
	public function getStatus() {
		return $this->estatus;
	}

	/**
	* establece el usuario
	* @param string $a el usuario
	*/
	public function setUser($a=NULL) {
		$this->user= ($a ? $a : self::RABBIT_USER);
	}

	/**
	* retorna el usuario
	* @return string el usuario
	*/
	public function getUser() {
		return $this->user;
	}

	/**
	* establece el password
	* @param string $a el password
	*/
	public function setPass($a=NULL) {
		$this->pass= ($a ? $a : self::RABBIT_PASS);
	}

	/**
	* retorna el password
	* @return string el password
	*/
	public function getPass() {
		return $this->pass;
	}

	/**
	* establece el error
	* @param string $a el error
	*/
	public function setError($a=NULL) {
		$this->error= ($a ? $a:NULL);
	}

	/**
	* retorna el error
	* @return string el error
	*/
	public function getError() {
		return $this->error;
	}

	/**
	* establece el exito
	* @param string $a el exito
	*/
	public function setSucess($a=NULL) {
		$this->sucess= ($a ? $a:NULL);
	}

	/**
	* retorna el exito
	* @return string el exito
	*/
	public function getSucess() {
		return $this->sucess;
	}

	/**
	* inicializa la conexion del socket y retorna el estatus
	* @return boolean falso o verdadero
	*/
	public function initRabbitMq() {
		$a= new AMQPStreamConnection($this->getServer(), $this->getPort(), $this->getUser(), $this->getPass());
		$this->socket= $a;
		$this->canal= $a->channel();
		$this->setSatus(is_object($this->socket) ? true:false);
		unset($a);
	}

	/**
	* envia un mensaje a la cola de rabbit
	* @param string $queue nombre de la cola
	* @param string $msg el mensaje
	*/
	public function send( $queue=NULL, $msg=NULL) {
		if( !$queue )
			$this->setError( "no indico la cola" );
		else if( !$msg )
			$this->setError( "no indico el mensaje");
		else {
			$c= $this->getCanal();
			$c->queue_declare($queue, false, false, false, false);
			$mensaje= new AMQPMessage($msg);

			if( !is_object($mensaje) ) {
				$this->setError("no se pudo crear el mensaje");
			}
			else {
				$c->basic_publish($mensaje, '', $queue);
				$this->setSucess("mensaje enviado");
			}
		}
	}

	/**
	* recepcion/consumo de un mensaje de la cola de rabbit
	* @param string $queue nombre de la cola
	*/
	public function reciv( $queue=NULL, $sleepTime=NULL, $callback=NULL, $autoAck=false ) {
		if( !$queue )
			$this->setError( "no indico la cola" );
		else {
			$c= $this->getCanal();
			$c->queue_declare($queue, false, false, false, false);
			// $callback = function ($msg) {
			//   echo "\n[x] Mensaje: ", $msg->body;
			//   echo "\n[*] esperando 10 segundos..";
			//   sleep(10);
			//   $msg->ack();
			//   echo "\n[*] ack de termino enviado a rabbit..";
			// };
			$espera= (($sleepTime && is_numeric($sleepTime)) ? $sleepTime:false);
			$c->basic_consume($queue, '', false, false, false, false, $callback);
			try {
			    $c->consume();
			} catch (\Throwable $exception) {
			    echo "\n\n===> ". $exception->getMessage();
			}
		}
	}

	/**
	* cierra la conexion hacia rabbit
	*/
	public function close() {
		// $c= $this->getCanal();
		// $c->close();

		// $s= $this->getSocket();
		// $s->close();

		unset($this->canal);
		unset($this->socket);
	}

	/**
	* retorna el socket file
	* @return array socket file
	*/
	public function getSocket() {
		return $this->socket;
	}

	/**
	* retorna canal file
	* @return array canal file
	*/
	public function getCanal() {
		return $this->canal;
	}

	/**
	* constructor
	* @param string $user el usuario
	* @param string $pass el password
	* @param string $host el servidor
	* @param string $port el puerto
	*/
	public function __construct($user=NULL, $pass=NULL, $host=NULL, $port=NULL) {
		$this->setUser($user);
		$this->setPass($pass);
		$this->setServer($host);
		$this->setPort($port);
	}
}
?>