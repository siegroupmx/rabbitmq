#!/usr/bin/php
<?php
include( "../autoload.php" );

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

$rabbitUser= 'angel';
$rabbitPass= 'gratis123';
$rabbitServer= NULL; // si no se indica, toma los de la libreria
$rabbitPort= NULL; // si no se indica, toma los de la libreria

// forma donde indicamos todos los campos que existen para la clase
// $rab= new rabbitMQ($rabbitUser, $rabbitPass, $rabbitServer, $rabbitPort);

// llamda regular donde tomo los datos por defecto de la clase "localhost" y puerto "5672"
$rab= new rabbitMQ($rabbitUser, $rabbitPass);

// inicializo la conexion, creamos el socket
$rab->initRabbitMq(); // abre conexion

if( !$rab->getStatus() ) {
	echo "\n[ERROR] La conexion no se realizo...";
}
else {
	echo "[*] Conexion exitosa a RabbitMQ..";
}

echo "\n\nFin del programa...\n\n";
exit(0);
?>