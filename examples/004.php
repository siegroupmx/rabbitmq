#!/usr/bin/php
<?php
/**
* Enviar un mensaje a la cola
*/
include( "../autoload.php" );

class Test {
	private $sleep= 5; // retardo

	// en respuesta de rabbit
	public function onCallback( $msg ) {
		if( !$msg->body_size ) {
			$this->onError();
		}
		else {
			echo "\n[x] Mensaje: ", $msg->body;
			echo "\n[*] ya estoy en la fuente [dormire ". $this->sleep. " segundos]...";
			sleep($this->sleep);
			$msg->ack();
			echo "\n[*] ack de termino enviado a rabbit..";
		}
	}

	public function onError() {
		echo "\nNo se encontraron datos...";
	}
}

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

$rabbitUser= 'angel';
$rabbitPass= 'gratis123';
$rabbitServer= NULL; // si no se indica, toma los de la libreria
$rabbitPort= NULL; // si no se indica, toma los de la libreria
$rabbitCola= 'demo'; // nombre de la cola

// forma donde indicamos todos los campos que existen para la clase
// $rab= new rabbitMQ($rabbitUser, $rabbitPass, $rabbitServer, $rabbitPort);

// llamda regular donde tomo los datos por defecto de la clase "localhost" y puerto "5672"
$rab= new rabbitMQ($rabbitUser, $rabbitPass);

// inicializo la conexion, creamos el socket
$rab->initRabbitMq(); // abre conexion

if( !$rab->getStatus() ) {
	echo "\n[ERROR] La conexion no se realizo...";
}
else {
	echo "[*] Conexion exitosa a RabbitMQ..";

	$sleepTime= 5;
	$obj= new Test();
	$rab->reciv($rabbitCola, $sleepTime, array($obj,"onCallback")); // no responde ACK en automatico

	if( $rab->getError() ) {
		echo "\n[ERROR] ". $rab->getError();
	}
	else {
		echo "\n[*] Mensaje recibido de la cola...";
	}
}

$rab->close();

echo "\n\nFin del programa...\n\n";
exit(0);
?>